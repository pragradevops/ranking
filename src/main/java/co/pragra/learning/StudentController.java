package co.pragra.learning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import co.pragra.learning.domain.Student;


import java.util.List;

@RestController
public class StudentController {

	@Autowired
	private StudentService service;


	private Student student;
	
	

	/**
	 * Rest API to create student
	 * end point would  /student
	 */

	@PostMapping(path = "/student", consumes = "application/json")
	public ResponseEntity<?> createStudent(@RequestBody Student student) {
		System.out.println(student);
		Student st = service.createStudent(student);
		if (null != st) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(st);
		}
		Error err = new Error();
		err.setCode("PR20");
		err.setMsg("Something went wrong during creation of entiry");
		return ResponseEntity.badRequest().body(err);

	}

	@RequestMapping(method = RequestMethod.GET,path = "/studentAll",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Student> getAllStudent(){
		return service.getAllStudents();
	}


	@RequestMapping(method = RequestMethod.PUT, path="students/{id}")
	public ResponseEntity<?> update(@PathVariable("id") Long id ,@RequestBody Student student){
		Student st= service.findById(id);
		if(null !=st) {
			
			st.setName(student.getName());
			st.setMarks(student.getMarks());
			Student st1 = service.createStudent(st);
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(st1);
		}
		Error err = new Error();
		err.setCode("PR21");
		err.setMsg("Please Enter Correct student id");
		return ResponseEntity.badRequest().body(err);
	}
	@RequestMapping(method = RequestMethod.GET,path = "/students/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public Student findById(@PathVariable("id") Long id){
		return service.findById(id);
	}



}





		class Error {
			private String msg;
			private String code;

			public String getMsg() {
				return msg;
			}

			public void setMsg(String msg) {
				this.msg = msg;
			}

			public String getCode() {
				return code;
			}

			public void setCode(String code) {
				this.code = code;
			}


		}




