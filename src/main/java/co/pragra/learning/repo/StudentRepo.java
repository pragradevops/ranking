package co.pragra.learning.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.pragra.learning.domain.Student;

@Repository
public interface StudentRepo extends JpaRepository<Student,Long> {
	//@Query("Select ALL from table_student where name like :name %")
	//List<Student> findByMatchName(@Param("name") String str);
}
