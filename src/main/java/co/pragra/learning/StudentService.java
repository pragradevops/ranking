package co.pragra.learning;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import co.pragra.learning.domain.Student;
import co.pragra.learning.repo.StudentRepo;

@Service
public class StudentService {
	
	public StudentRepo repository;

	public StudentService(StudentRepo repository) {
		this.repository = repository;
	}
	
	public Student createStudent(Student s) {
		return this.repository.save(s);
	}
	
	public List<Student> getAllStudents(){
		return this.repository.findAll();
	}
	
	public Student update(Student s) {
		return this.repository.save(s);
	}

	
	public Student findById(Long id) {
		return this.repository.findById(id).get();
		
	}

//	public List<Student> getAllStudentsByFirstNameMatch(String name){
//		return this.repository.findByMatchName(name);
//	}
	

}
